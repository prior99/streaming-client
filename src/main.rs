#[macro_use] extern crate clap;
extern crate websocket;
extern crate opus;
#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate serde_json;

use std::sync::mpsc::{channel, Sender};
use std::fs::File;
use std::thread;
use opus::{Decoder, Encoder, Channels, Error, Application};
use ogg::{PacketReader};
use websocket::client::ClientBuilder;
use websocket::{OwnedMessage};

const FRAME_DURATION_MS: usize = 80;

struct StreamingClient {
    encoder: Encoder,
    sample_rate: usize,
    current_buffer: Vec<f32>,
    tx: Sender<Vec<u8>>,
}

#[derive(Deserialize, Serialize)]
struct Info {
    pub sample_rate: usize,
    pub channels: usize,
}

impl StreamingClient {
    pub fn new(sample_rate: usize, url: String) -> Result<StreamingClient, Error> {
        let (tx, rx) = channel();
        thread::spawn(move || {
            let mut websocket_client = ClientBuilder::new(&url)
                .unwrap()
                .add_protocol("ogg-opus")
                .connect_insecure()
                .unwrap();
            let info = Info {
                sample_rate,
                channels: 2,
            };
            let info_json = serde_json::to_string(&info).unwrap();
            websocket_client.send_message(&OwnedMessage::Text(info_json)).unwrap();
            loop {
                match rx.recv() {
                    Ok(data) => websocket_client.send_message(&OwnedMessage::Binary(data)).unwrap(),
                    Err(e) => panic!("{:?}", e),
                }
            }
        });
        let client = StreamingClient {
            encoder: Encoder::new(sample_rate as u32, Channels::Stereo, Application::Audio)?,
            sample_rate,
            current_buffer: vec![],
            tx,
        };
        Ok(client)
    }

    pub fn process_pcm(&mut self, data: Vec<f32>) {
        self.current_buffer.extend(data);
        println!("Appending to data, now {}", self.current_buffer.len());
        while self.current_buffer.len() >= self.frame_size() as usize {
            let slice = self.current_buffer.drain(0..self.frame_size()).collect();
            self.encode(slice);
        }
    }

    fn frame_size(&self) -> usize {
        FRAME_DURATION_MS * (self.sample_rate / 1000) * 2
    }

    fn encode(&mut self, data: Vec<f32>) {
        println!("Encoding buffer of size {}.", data.len());
        let encoded = self.encoder.encode_vec_float(&data, self.frame_size()).unwrap();
        println!("Encoded it's {}", encoded.len());
        self.tx.send(encoded).unwrap();
    }
}

fn main() {
    use clap::App;
    let yml = load_yaml!("commandline.yml");
    let matches = App::from_yaml(yml).get_matches();
    let file_path = matches.value_of("file").expect("No file specified.");
    let url = matches.value_of("url").expect("No URL specified.");

    let mut file = File::open(file_path).expect("Can't open file.");
    let mut ogg_reader = PacketReader::new(&mut file);
    let mut opus_decoder = Decoder::new(48000, Channels::Stereo).unwrap();
    let mut packet_number = 0;
    let mut client = StreamingClient::new(48000, String::from(url)).unwrap();
    loop {
        packet_number += 1;
        match ogg_reader.read_packet().unwrap() {
            Some(packet) => {
                if packet_number < 3 { continue; }
                let samples = opus_decoder.get_nb_samples(&packet.data).unwrap_or(0);
                let mut decoded_buffer = vec![0f32; samples * 2];
                opus_decoder.decode_float(&packet.data, &mut decoded_buffer, false).unwrap();
                client.process_pcm(decoded_buffer);
            },
            None => break,
        }
        thread::sleep(std::time::Duration::from_millis(10));
    }
}
